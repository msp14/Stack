package stack;

/**
 * Stack Implementation
 * Author: Marcel Spitzer
 * Date: 7/11/15
 */
public class CStack<T> implements IStack<T>
{
    /**
     * top element
     */
    private Element top = null;

    /**
     * ctor initializes an empty stack with root element
     */
    public CStack()
    {
    }

    @Override
    public final void push( final T value )
    {
        // create a new stack element and update reference
        top = new Element( value, top );
    }

    @Override
    public final T pop()
    {
        if ( this.isEmpty() )
            return null;

        // get top value
        final T topValue = top.getValue();

        // update references
        top = top.getPrevious();

        return topValue;
    }

    @Override
    public final T top()
    {
        if ( this.isEmpty() )
            return null;

        return top.getValue();
    }

    @Override
    public final boolean isEmpty()
    {
        return top == null;
    }

    @Override
    public String toString()
    {
        String result = "";

        for ( Element current = top; !this.isEmpty(); current = current.getPrevious() )
            result = result.concat(current.getValue() + " -> ");

        return result.concat( "ROOT" );
    }

    /**
     * class for stack elements
     */
    private class Element
    {
        /**
         * element value
         */
        private final T value;

        /**
         * previous element
         */
        private final Element previous;

        /**
         * ctor
         *
         * @param value element value
         * @param previous predecessor element
         */
        public Element( final T value, final Element previous )
        {
            if( value == null )
                throw new IllegalArgumentException("value has to be specified");

            this.value = value;
            this.previous = previous;
        }

        /**
         * getter for value
         *
         * @return element value
         */
        public final T getValue()
        {
            return value;
        }

        /**
         * getter for previous element
         *
         * @return previous element
         */
        public final Element getPrevious()
        {
            return previous;
        }
    }
}


