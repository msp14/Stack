package stack;

/**
 * Stack Interface
 * Author: Marcel Spitzer
 * Date: 7/11/15
 */
public interface IStack<T>
{
    /**
     * push a new value on top of the stack
     *
     * @param value new value
     */
    void push(T value);

    /**
     * return and remove top value
     *
     * @return top value
     */
    T pop();

    /**
     * return top value
     *
     * @return top value
     */
    T top();

    /**
     * check if stack is empty
     *
     * @return true if emtpy
     */
    boolean isEmpty();
}
