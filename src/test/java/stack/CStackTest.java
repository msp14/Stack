package stack;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Stack test routines
 * @author Marcel Spitzer
 * @date 7/11/15
 */
public class CStackTest
{
    @Test
    public void testStack() throws Exception
    {
        final IStack<Integer> stack = new CStack<Integer>();

        assertTrue("stack has to be initialized empty", stack.isEmpty());

        stack.push(1);
        assertEquals(stack.top(), new Integer(1));
        assertFalse("stack must not be emtpy", stack.isEmpty());

        stack.push(2);
        assertEquals(stack.top(), new Integer(2));
        assertEquals( stack.pop(), new Integer(2) );
        assertEquals( stack.pop(), new Integer(1) );

        assertTrue("stack must be empty", stack.isEmpty());
        assertEquals(stack.top(), null);

    }
}
